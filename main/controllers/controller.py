#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Import the partial module"""
from functools import partial


__author__ = "Ivan de Paz Centeno"


def route(*args, **kwargs):
    """
    Decorator proxy for @self.flask_web_app.route.
    Since we cannot use "self" while calling the decorator, proxying it is a good solution.
    :param args:
    :param kwargs:
    :return:
    """
    def decorator1(func):
        """
        Decorator proxy
        """

        def decorator2(self):
            """
            Decorator proxy
            """

            app = self.flask_web_app
            # At this level we have the app defined, extracted from self.

            partial_func = partial(func, self)
            app.add_url_rule(*(args + ("{}.{}".format(self.__class__.__name__, func.__name__), partial_func)), **kwargs)

        return decorator2

    return decorator1


def errorhandler(*args, **kwargs):
    """
    Decorator proxy for @self.flask_web_app.errorhandler.

    Since we cannot use "self" while calling the decorator, proxying it is a good solution.
    :param args:
    :param kwargs:
    :return:
    """

    def decorator1(foo):
        """
        Decorator Proxy
        :param foo:
        :return:
        """
        def decorator2(self):
            """
            Decorator Proxy
            :param self:
            :return:
            """
            app = self.flask_web_app

            # At this level we have the app defined, extracted from self.
            @app.errorhandler(*args, **kwargs)
            def proxy_method(error):
                return foo(self, error)

            return proxy_method

        return decorator2

    return decorator1


class Controller(object):
    """
    Generic controller super class.
    Inherit it to build a controller.
    """
    def __init__(self, flask_web_app):
        self.flask_web_app = flask_web_app

