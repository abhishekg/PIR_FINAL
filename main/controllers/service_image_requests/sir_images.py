#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Imports the controller """
from main.controllers.controller import Controller, route, errorhandler
from flask import jsonify
from flask import request
from main.exceptions.InvalidRequest import InvalidRequest
from werkzeug.utils import secure_filename
import os
import json
from main.model.resource.Resources import SupportMethods
from main.model.resource.service_routines import ServiceRoutines


__author__ = "Abhishek Gangwar"


class ServiceImageRecognitionController(Controller):
    """
    Controller for Service Images Recognition system services
    :param :
    :return:
    """
    def __init__(self, flask_web_app, web_app_config):

        Controller.__init__(self, flask_web_app)
        self.web_app_config = web_app_config
        self.datafile = self.web_app_config.get('datafile')
        self.saveimagepath = self.web_app_config.get('phishing_image_path')

        self.exposed_methods = [
            self.get_available_tasks,
            self.list_services,
            self.detect_service,
            self.delete_service,
            self.add_service,
            self.add_service_path,
            self.detect_service_path,
            self.handle_invalid_request
        ]
        # Let's init the exposed methods
        [exposed_method() for exposed_method in self.exposed_methods]

    @route("/PIR/tasks", methods=['GET'])
    def get_available_tasks(self):
        """
        provides the services available for Service Image Classification System.
        :return: List of the services of the system in Json format
        """
        services = []
        srvc = {
            'Task Type': "List Available Endpoints In the System",
            'Task URL':  "curl -X GET http://" + self.web_app_config.get('ip') + ":" +
                         self.web_app_config.get('port') + "/PIR/tasks"
        }
        services.append(srvc)
        srvc = {
            'Task Type': "Detect Phishing Image from Image file",
            'Task URL': "curl -X PUT -F imagefile=@<IMAGEFILE NAME>  http://" + self.web_app_config.get('ip') + ":"
                        + self.web_app_config.get('port') + "/PIR/phishing"
        }
        services.append(srvc)
        srvc = {
            'Task Type': "Detect Phishing type by passing Imagepath",
            'Task URL': "curl -X PUT -F imagepath=<IMAGEPATH>  http://" + self.web_app_config.get('ip') + ":"
                        + self.web_app_config.get('port') + "/PIR/phishing/detect_imagepath"
        }
        services.append(srvc)
        srvc = {
            'Task Type': "Add Phishing Panel/Image by passing actual image file from client",
            'Task URL': "curl -X POST - F phishing_name=<PHISHING NAME> -F phishing_type=<PHISHING TYPE> -F " +
                        "phishing_description=<PHISHING DESCRIPTION> " + "imagefile=@<IMAGEFILE>  http://" +
                        self.web_app_config.get('ip') + ":" + self.web_app_config.get('port') + "/PIR/phishing"
        }
        services.append(srvc)
        srvc = {
            'Task Type': "Add Phishing Panel/Image by passing imagepath on the server",
            'Task URL': "curl -X POST - F phishing_name=<PHISHING NAME> -F phishing_type=<PHISHING TYPE> -F " +
                        "phishing_description=<PHISHING DESCRIPTION> " + "imagepath=<IMAGEPATH> " +
                        "http://" + self.web_app_config.get('ip') + ":" +
                        self.web_app_config.get('port') + "/PIR/phishing/add_imagepath"
        }
        services.append(srvc)
        srvc = {
            'Task Type': "Delete Phishing Panel/Image",
            'Task URL':  "curl -X DELETE -F phishing_id=<PHISHING ID> " +
                         "http://" + self.web_app_config.get('ip') + ":" +
                         self.web_app_config.get('port') + "/PIR/phishing"
        }
        services.append(srvc)
        srvc = {
            'Task Type': "List available Phishing Panel/Image",
            'Task URL':  "curl -X GET http://" + self.web_app_config.get('ip') + ":" +
                         self.web_app_config.get('port') + "/PIR/phishing"
        }
        services.append(srvc)
        return jsonify({'List':services})

    @route('/PIR/phishing', methods=['PUT'])
    def detect_service(self):
        """
        provides the service image detection service.
        :return: decision on input image for Service image or not in Json format
        """
        file = request.files['imagefile']
        datafile = self.datafile
        filename = secure_filename(file.filename)
        image = SupportMethods().read_preprocess_image(file)
        dt = ServiceRoutines()
        out = dt.detect_image(datafile, filename, image)
        if out == 0:
            return jsonify({'phishing': 0})

        else:
            return jsonify({'phishing': 1,
                            'phishing_id': out[0],
                            'phishing_name': out[1],
                            'phishing_type': out[2],
                            'phishing_description': out[3]})


    @route('/PIR/phishing/detect_imagepath', methods=['PUT'])
    def detect_service_path(self):
        """
        provides the service image detection service.
        :return: decision on input image for Service image or not in Json format
        """
        datafile = self.datafile
        filepath = request.form['imagepath']
        if not os.path.isfile(filepath):
            raise InvalidRequest("The Test Image Does not Exist")

        filename = os.path.basename(filepath)
        image = SupportMethods().read_preprocess_image(filepath)

        dt = ServiceRoutines()
        out = dt.detect_image(datafile, filename, image)
        if out == 0:
            return jsonify({'phishing': 0})

        else:
            return jsonify({'phishing': 1,
                            'phishing_id': out[0],
                            'phishing_name': out[1],
                            'phishing_type': out[2],
                            'phishing_description': out[3]})


    @route('/PIR/phishing/add_imagepath', methods=['POST'])
    def add_service_path(self):
        """
        provides the service image add service thru IMAGE PATH
        :return: Stats of new Service image addition in Json format
        """
        if request.method != "POST":
            raise InvalidRequest("The Request is not POST")

        datafile = self.datafile
        saveimagepath = self.saveimagepath
        phishing_name = request.form['phishing_name']
        phishing_type = request.form['phishing_type']
        phishing_description = request.form['phishing_description']

        filepath = request.form['imagepath']
        filename = os.path.basename(filepath)
        image = SupportMethods().read_preprocess_image(filepath)

        add1 = ServiceRoutines()
        phishing_id = add1.add_new_record(datafile, saveimagepath, phishing_name, phishing_type, phishing_description, filename, image)

        return jsonify({'phishing': '1',
                        'phishing_id': phishing_id,
                        'phishing_name': phishing_name,
                        'phishing_type': phishing_type,
                        'phishing_description': phishing_description}), 201

    @route('/PIR/phishing', methods=['POST'])
    def add_service(self):
        """
        provides the service image add service.
        :return: Stats of new Service image addition in Json format
        """
        if request.method != "POST":
            raise InvalidRequest("The Request is not POST")
        datafile = self.datafile
        saveimagepath = self.saveimagepath
        phishing_name = request.form['phishing_name']
        phishing_type = request.form['phishing_type']
        phishing_description = request.form['phishing_description']
        file = request.files['imagefile']
        filename = secure_filename(file.filename)

        image = SupportMethods().read_preprocess_image(file)

        add1 = ServiceRoutines()
        phishing_id = add1.add_new_record(datafile, saveimagepath, phishing_name, phishing_type, phishing_description,
                                         filename, image)

        return jsonify({'phishing': '1',
                        'phishing_id': phishing_id,
                        'phishing_name': phishing_name,
                        'phishing_type': phishing_type,
                        'phishing_description': phishing_description}), 201

    @route('/PIR/phishing', methods=['DELETE'])
    def delete_service(self):
        """
        provides the service image deletion service.
        :return: status of input image deletion in Json format
        """
        datafile = self.datafile
        phishing_id = request.form['phishing_id']
        f = open(datafile, 'r')
        data = json.load(f)
        if len(data) > 0:
            out = SupportMethods()
            out = out.verify_record_is_in_data(data, int(phishing_id))
            if out == 1:
                record = [tk for tk in data if int(tk['phishing_id']) == int(phishing_id)]
                data.remove(record[0])
                f.close()
                f1 = open(datafile, 'w')
                json.dump(data, f1)
                f1.close()
                imagename = record[0]['filepath']
                if os.path.exists(imagename):
                    os.remove(imagename)
                    flag = '1'
                else:
                    flag= '0'

                return jsonify({'phishing deleted': '1',
                                'phishing_id': phishing_id,
                                'phishing_image_deleted': flag }), 201
            else:
                return jsonify({'result': 'Record not found'})
        else:
            return jsonify({'result': 'Data not found'})

    @route('/PIR/phishing', methods=['GET'])
    def list_services(self):
        """
        provides the list of available service images.
        :return: list of registered Service images in Json format
        """
        datafile = self.datafile
        f = open(datafile, 'r')
        data = json.load(f)
        service_list = []
        if len(data) > 0:
            for i in range(len(data)):
                record = {'Template_stored': data[i].get('filepath'),
                          'phishing_id': data[i].get('phishing_id'),
                          'phishing_name': data[i].get('phishing_name'),
                          'phishing_type': data[i].get('phishing_type'),
                          'phishing_description': data[i].get('phishing_description')
                          }
                service_list.append(record)
            return jsonify({'phishing_List': service_list})
        else:
            return jsonify({'result': 'Data is Empty'})

    @errorhandler(InvalidRequest)
    def handle_invalid_request(self, error):
        """
        Handles the invalid request error in order to formally notify the appropriate code.
        :param error: dict containing the explanation of the error.
        :return:
        """
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response
