#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Author Name"""
__author__ = "Ivan de Paz Centeno"


class InvalidRequest(Exception):
    """
    This Class Handles Invalid Requests
    """
    def __init__(self, message, status_code=400, payload=None):
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv
