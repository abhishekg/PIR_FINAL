#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Imports the flask module """
from flask import Flask
import json
from main.controllers.service_image_requests.sir_images import ServiceImageRecognitionController

__author__ = "Abhishek Gangwar"


DEFAULT_CONFIG_FILE = 'main/etc/configuration.json'

with open(DEFAULT_CONFIG_FILE, 'r') as f:
    web_app_config = json.load(f)

app = Flask(__name__)

controllers = [
    ServiceImageRecognitionController(app, web_app_config),
]


app.run(web_app_config.get('ip'), int(web_app_config.get('port')))
