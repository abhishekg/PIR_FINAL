#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Imports"""
import numpy as np
from PIL import Image
import imghdr
from flask import jsonify
from main.exceptions.InvalidRequest import InvalidRequest

__author__ = "Abhishek Gangwar"


class SupportMethods(object):
    """
    provides the various methods required for specific tasks.
    """

    def __init__(self):
        """
        provides the various methods required for specific tasks.
        """
    def read_preprocess_image(self, filepath):
        """
        This method reads the image and does preprocessing
        :return: preproprocessed Image
        """
        if imghdr.what(filepath) == None:
            raise InvalidRequest("The Image file is not valid")
        try:
            image = Image.open(filepath)
            width, height = image.size
            if height > 0.75*width:
                image = image.crop((0, 0, int(width), int(0.75*width)))

            if image.mode == 'RGBA':
                x = np.array(image)
                r, g, b, a = np.rollaxis(x, axis=-1)
                r[a == 0] = 255
                g[a == 0] = 255
                b[a == 0] = 255
                x = np.dstack([r, g, b, a])
                image = Image.fromarray(x, 'RGBA')
        except IOError:
            raise InvalidRequest("The Image file is not valid")

        return image

    def verify_record_is_in_data(self,data,phishing_id):
        """
        verifies whether the service id is already used.
        """
        for i in range(len(data)):
            if int(data[i].get('phishing_id')) == int(phishing_id):
                out = 1
                break

            else:
                out = 0
        return int(out)

    def hamming2(self,s1,s2):
        """
        provides the hamming distance between two hex strings.
        """
        # convert hex strings to binary
        s1 = bin(int('1'+s1, 16))[3:]
        s2 = bin(int('1'+s2, 16))[3:]
        # Calculate the Hamming distance between two bit strings
        assert len(s1) == len(s2)
        return sum(c1 != c2 for c1, c2 in zip(s1, s2))

    def get_phishing_id(self,data):
        if len(data) == 0:
            phishing_id = 0
        else:
            ids = []
            for i in range(len(data)):
                ids.append(int(data[i].get('phishing_id')))
                phishing_id = max(ids)+1
        return phishing_id

    def is_image(self, filename):
        f = filename.lower()

        return f.endswith(".png") or f.endswith(".jpg") or \
                   f.endswith(".jpeg") or f.endswith(".bmp")

