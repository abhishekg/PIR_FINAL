#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Imports the json """
import json
from main.exceptions.InvalidRequest import InvalidRequest
import os
import imagehash
from flask import jsonify
from main.model.resource.Resources import SupportMethods

__author__ = "Abhishek Gangwar"


class ServiceRoutines(object):
    """
    provides the various methods required for specific tasks.
    """

    def __init__(self):
        """
        provides the various methods required for specific tasks.
        """

    def add_new_record(self, datafile, saveimagepath, phishing_name, phishing_type, phishing_description, filename, image):
        """
        adds the new record into the datafile.
        """

        if not SupportMethods().is_image(filename):
            raise InvalidRequest("Error In Adding Image': 'The allowed Image Extensions are: .png, .jpg, .jpeg, .bmp'")

        if len(phishing_description) > 200:
            raise InvalidRequest("The service description has " + str(len(phishing_description)) +
                                 " letters. The maximum allowed limit is 200 letters", 405)
        savefile = os.path.join(saveimagepath, filename)
        image.save(savefile)

        phash = str(imagehash.phash(image))

        # Create data file if does not exist
        if not os.path.exists(datafile):
            drec = []
            f = open(datafile, 'w')
            json.dump(drec, f)
            f.close()
            f = open(datafile, 'r')
            data = json.load(f)
        else:
            f = open(datafile, 'r')
            data = json.load(f)
        f.close()

        phishing_id = SupportMethods().get_phishing_id(data)
        # print(phishing_id)

        record = {
            'phishing_id': phishing_id,
            'phishing_type': phishing_type,
            'phishing_name': phishing_name,
            'phishing_description': phishing_description,
            'phash': phash,
            'filepath': savefile
        }
        # Add first Record
        f = open(datafile, 'r')
        data = json.load(f)
        if len(data) == 0:
            f.close()
            record = [record]
            f1 = open(datafile, 'r')
            json.dump(record, f1)
            f1.close()
        else:
            # Add New Records
            f1 = open(datafile, 'w')
            data.append(record)
            json.dump(data, f1)

        return phishing_id

    def detect_image(self, datafile, filename, image):
        """
        detects the test image as service image or not
        """
         
        # verify Image Extension
        if not SupportMethods().is_image(filename):
            raise InvalidRequest("Error In Adding Image': 'The allowed Image Extensions are: .png, .jpg, .jpeg, .bmp'")

        h1 = str(imagehash.phash(image))
        threshp = 10
        f = open(datafile, 'r')
        data = json.load(f)
        if len(data) > 0:
            pdist = []
            for i in range(len(data)):
                ds1 = SupportMethods().hamming2(data[i].get('phash'), h1)
                pdist.append(ds1)
        else:
            return jsonify({'Data Is Empty': ''})

        if min(pdist) < threshp:
            indx = pdist.index(min(pdist))
            phishing_type = data[indx].get('phishing_type')
            phishing_id = data[indx].get('phishing_id')
            phishing_name = data[indx].get('phishing_name')
            phishing_description = data[indx].get('phishing_description')

            out = [phishing_id, phishing_name, phishing_type, phishing_description]

        else:
            out = 0

        return out
