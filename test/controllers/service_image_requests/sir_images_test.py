#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import base64
import json
import unittest
import flask
from flask import request

from main.controllers.service_image_requests.sir_images import ServiceImageRecognitionController

__author__ = 'Abhishek Gangwar'

class ServiceImageRecognitionControllerTests(unittest.TestCase):
    """
    Tests the class controller ServiceImageRecognitionController
    """

    def setUp(self):
        self.app = flask.Flask(__name__)
        self.app.config['TESTING'] = True
        DEFAULT_CONFIG_FILE = 'main/etc/configuration.json'

        with open(DEFAULT_CONFIG_FILE, 'r') as f:
            web_app_config = json.load(f)
        self.service_image_recognition_controller = ServiceImageRecognitionController(self.app, web_app_config)
        self.task_list_url = "/PIR/tasks"
        self.service_main_url = "/PIR/phishing"
        self.service_detect_path_url = "/PIR/phishing/detect_imagepath"
        self.service_add_url = "/PIR/phishing/add_imagepath"
        self.client = self.app.test_client()

    def test_get_available_tasks(self):
        """
        Tests the url for listing available tasks in the system

        """
        response = self.client.get(self.task_list_url)
        self.assertEquals(response.status_code, 200)

    def test_list_services(self):
        """
        Tests the url for listing available tasks in the system

        """
        response = self.client.get(self.service_main_url)
        self.assertEquals(response.status_code, 200)
