#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import unittest
from main.model.resource.Resources import SupportMethods
import imagehash
from PIL import Image
import json

__author__ = 'Abhishek Gangwar'


class SupportMethodsTests(unittest.TestCase):

    def test_hamming2(self):
        """
        This is test of groundtruth hamming distance against generated hamming distance
        :return: 
        """
        hash1 = '71ef74430fc42e16'
        hash2 = '31d826a7d972d926'
        hd = SupportMethods().hamming2(hash1, hash2)
        self.assertEqual(hd, 32)

    def test_phash(self):
        """
        This is test of groundtruth hash against generated phash
        :return: 
        """
        image1 = Image.open('main/samples/image1.jpeg')
        phash1 = '71ef74430fc42e16'
        hash1 = str(imagehash.phash(image1))
        self.assertEquals(phash1, hash1)

    def test_is_image(self):
        """
        To test the input image extension is from allowed extensions
        :return: 
        """
        filename1 = 'image1.jpeg'
        filename2 = 'image1.pdf'

        self.assertTrue(SupportMethods().is_image(filename1))
        self.assertFalse(SupportMethods().is_image(filename2))

    def test_get_phishing_id(self):
        dummydatafile= 'main/samples/dummydata.json'
        current_id = 2
        dummyrecord = [{
            'phishing_id': current_id,
            'phishing_type': 'NONE',
            'phishing_name': 'Abanca',
            'phishing_description': 'Abanca_NONE',
            'phash': '77a90e9c761ea05c',
            'filepath': 'main/data/images/1_1072409.png'
			
        }]
        with open(dummydatafile, 'w') as f1:
            json.dump(dummyrecord, f1)
        f1.close()
        with open(dummydatafile, 'r') as f:
            dummydata = json.load(f)
        f.close()
        phishing_id = SupportMethods().get_phishing_id(dummydata)
        self.assertEquals(phishing_id, (current_id+1))


if __name__ == '__main__':
    unittest.main()
