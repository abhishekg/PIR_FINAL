#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
from main.model.resource.Resources import SupportMethods
from PIL import Image
import os
import json
from main.model.resource.service_routines import ServiceRoutines

__author__ = 'Abhishek Gangwar'

class SupportMethodsTests(unittest.TestCase):

    def test_add_new_record(self):
        """
        This is test of groundtruth hamming distance against generated hamming distance
        :return: 
        """
        datafile = 'main/samples/dummydata.json'
        saveimagepath = 'main/samples/'
        service_name = 'dummy Service'
        service_type = 'dummy service type'
        service_description = 'this is dummy service for unittest purpose'
        filepath = 'main/samples/image3.jpeg'
        filename = os.path.basename(filepath)
        image = Image.open(filepath)

        f = open(datafile, 'r')
        data = json.load(f)
        f.close()
        service_id1 = SupportMethods().get_phishing_id(data)

        add1 = ServiceRoutines()
        service_id2 = add1.add_new_record(datafile, saveimagepath, service_name, service_type, service_description, filename, image)
        self.assertEquals(service_id1, service_id2)

    def test_detect_image(self):
        datafile = 'main/samples/dummydata.json'
        saveimagepath = 'main/samples/'
        service_name = 'dummy Service'
        service_type = 'dummy service type'
        service_description = 'this is dummy service for unittest purpose'
        filepath = 'main/samples/image3.jpeg'
        filename = os.path.basename(filepath)
        image = Image.open(filepath)

        add1 = ServiceRoutines()
        service_id = add1.add_new_record(datafile, saveimagepath, service_name, service_type, service_description,
                                          filename, image)

        dt = ServiceRoutines()
        out = dt.detect_image(datafile, filename, image)
        self.assertEquals(out[2], service_type)



if __name__ == '__main__':
    unittest.main()